# Hello~
This page serves as a recruitment page for Assistant Professor Roy Ka-Wei Lee from Singapore University of Technology and Design (SUTD).

Roy's research lies in the intersection of data mining, machine learning, social computing, and natural language processing. His research goal is to understand user behaviors and design the data-driven systems and algorithms for improving user experiences in online social platforms (OSPs). Specifically, his research studio focus on the following research themes:
- Online Misbehavior and Disinformation Mining
- Social Recommender Systems
- Social Natural Language Generation

Currently, there are Research Assistant and Research Fellow positions available. If you are interested in the positions, please send him an email at **roy_lee[at]sutd.edu.sg**.

---

###  Research Assistant

**Responsibilities**

The candidate will also be responsible for one or more of the following aspects:

- Develop new data analytics and crawling capabilities
- Develop and maintain real-time data analytics services and systems
- Create prototype/application to showcase centre’s analytics capabilities
- Demonstrate applications at conferences
- Assist other researchers to collect and extract relevant data

**Requirements**
- Bachelor/Master Degree in Computer Science/Engineering or relevant disciplines
- Good knowledge of Databases, Data Mining, Distributed Computing, Operating Systems
- Proficient in at least one or more of these programming languages: Java, Python, C#, C++, C
- Self-motivated and able to carry out tasks independently
- Excellent team player and possess strong inter-personal skills
- Experience in the following technologies is an advantage:
    - Elasticsearch
    - Apache Storm
    - MySQL, Postgresql
    - Tomcat, NodeJS, Apache, Nginx and Xampp
    - Linux Environment (CentOS 6.x, Ubuntu 14.x), Bash Scripting
    - Virtualization (VMWare, KVM, Vagrant, Docker)
    - GPU Computing and Deep Learning


---

### Research Fellow/ Post-Doc

**Responsibilities**

The Research Fellow/Scientist will work with faculty researchers on one or more of the following research topics:

- Data and Text Mining
- Social Computing
- Machine Learning
- Deep Learning
- Recommender Systems
- Natural Language Processing

The candidate will also be responsible for one or more of the following aspects:
- Design research methodologies
- Develop algorithms and models
- Conduct data science studies
- Design and conduct experiments
- Design and implement applications, tools and systems
- Develop analytics capabilities in big data infrastructure

**Requirements**

- PhD in Computer Science or related disciplines
- Good knowledge in relevant research topics and possess related research skills
- Proficient in at least one or more of these programming languages: Java, Python, C#, C++ and C
- Self-motivated and able to carry out tasks independently
- Excellent team player and possess strong inter-personal skills
- Analytical and meticulous with attention to details
